/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   binary_heap.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mcizo <mcizo@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/26 22:26:37 by mcizo             #+#    #+#             */
/*   Updated: 2016/02/08 16:20:02 by mcizo            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <lemin.h>

/*
** http://chgi.developpez.com/arbre/tas/
** http://robin-thomas.github.io/min-heap/
** http://www.tutorialspoint.com/data_structures_algorithms/
** https://www.cs.cmu.edu/~tcortina/15-121sp10/Unit06B.pdf
*/

int				heap_left_child(int index)
{
	return (2 * index);
}

int				heap_right_child(int index)
{
	return ((2 * index) + 1);
}

int				heap_parent(int index)
{
	return (index / 2);
}

static void		heap_realloc(t_heap *heap)
{
	t_room	**new_array;
	int		new_size;
	int		i;

	new_size = heap->capacity + 100;
	new_array = ft_memalloc(new_size * sizeof(t_room *));
	if (new_array == NULL)
		ft_putendl("ERROR");
	i = 0;
	while (i <= heap->size)
	{
		new_array[i] = heap->array[i];
		i++;
	}
	free(heap->array);
	heap->array = new_array;
	heap->capacity = new_size;
}

void			insert_heap_node(t_heap *heap, t_room *node)
{
	int		i;
	int		parent;
	t_room	*tmp;

	if (heap->size >= heap->capacity)
		heap_realloc(heap);
	heap->size++;
	i = heap->size;
	parent = heap_parent(i);
	heap->array[i] = node;
	node->enqueued = TRUE;
	node->index = i;
	if (heap->size > 1)
	{
		while (i > 1
			&& heap->array[i]->dist < heap->array[parent]->dist)
		{
			tmp = heap->array[parent];
			heap->array[parent] = heap->array[i];
			heap->array[i] = tmp;
			i = parent;
			parent = heap_parent(i);
		}
	}
}
