/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   bin_heap2.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mcizo <mcizo@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/28 22:33:45 by mcizo             #+#    #+#             */
/*   Updated: 2016/02/08 16:53:31 by mcizo            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <lemin.h>

t_heap			*init_min_heap(void)
{
	t_heap	*heap;

	heap = ft_memalloc(sizeof(t_heap));
	if (heap != NULL)
	{
		heap->array = ft_memalloc(10 * sizeof(t_room *));
		heap->capacity = 8;
		heap->size = 0;
	}
	return (heap);
}

void			free_heap(t_heap *heap)
{
	free(heap->array);
	free(heap);
}

static void		heap_swap(t_heap *heap, int a, int b)
{
	t_room	*tmp;

	tmp = heap->array[a];
	heap->array[a] = heap->array[b];
	heap->array[b] = tmp;
}

void			heapify(t_heap *heap, int i)
{
	int		left;
	int		right;

	left = heap_left_child(i);
	right = heap_right_child(i);
	if (left > heap->size)
		return ;
	if (right <= heap->size
		&& heap->array[right]->dist < heap->array[left]->dist)
	{
		heap_swap(heap, right, i);
		heapify(heap, right);
	}
	else
	{
		heap_swap(heap, left, i);
		heapify(heap, left);
	}
}

void			heap_delete_root(t_heap *heap)
{
	heap->array[1] = heap->array[heap->size];
	heap->array[heap->size] = NULL;
	heap->size -= 1;
	heapify(heap, 1);
}
