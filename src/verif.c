/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   verif.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mcizo <mcizo@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/15 03:03:18 by mcizo             #+#    #+#             */
/*   Updated: 2016/01/26 22:29:09 by mcizo            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <lemin.h>

t_bool	verif_split_room(char **split)
{
	int	i;
	int	i2;

	i = 1;
	while (i < 3)
	{
		i2 = 1;
		if (split[i][0] != '-' && ft_isdigit(split[i][0]) == 0)
			return (FALSE);
		while (split[i][i2] != '\0' && ft_isdigit(split[i][i2]) != 0)
			i2++;
		if (split[i][i2] != '\0')
			return (FALSE);
		i++;
	}
	return (TRUE);
}

t_bool	verif_split_tube(char **split)
{
	int		i;

	i = 0;
	while (split[i] != 0)
		i++;
	if (i != 2)
	{
		ft_array_del(split);
		return (FALSE);
	}
	return (TRUE);
}

t_bool	verif_new_tube(t_list *tube_list, char *new_tube)
{
	t_elem	*current;

	current = tube_list->head;
	while (current != NULL && ft_strcmp(current->room->name, new_tube) != 0)
		current = current->next;
	if (current == NULL)
		return (TRUE);
	else
		return (FALSE);
}
