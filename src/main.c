/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mcizo <mcizo@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/12 15:58:37 by mcizo             #+#    #+#             */
/*   Updated: 2016/02/08 17:18:05 by mcizo            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <lemin.h>

static t_bool	has_cmd(t_list *room_list, t_type command, t_elem **room)
{
	t_elem	*current;

	current = room_list->head;
	while (current != NULL)
	{
		if (current->room->type == command)
		{
			*room = current;
			break ;
		}
		current = current->next;
	}
	if (current == NULL)
	{
		*room = NULL;
		return (FALSE);
	}
	return (TRUE);
}

t_type			check_line(char *line, t_type *current, t_list *room_list)
{
	if (ft_strlen(line) == 0)
		return (STOP);
	if (line[0] == '#' && (line[1] == '\0' || line[1] != '#'))
		return (COMMENT);
	else if (check_number(line) == TRUE)
		return (NUMBER);
	else if (ft_strcmp(line, "##start") == 0 && check_cmd(CMD_START, line))
		return (CMD_START);
	else if (ft_strcmp(line, "##end") == 0 && check_cmd(CMD_END, line))
		return (CMD_END);
	else if (ft_strncmp(line, "##", 2) == 0)
		return (CMD_ERROR);
	else if (check_room(line, current, room_list) == TRUE)
		return (ROOM);
	else if (check_tube(line, current, room_list) == TRUE)
		return (TUBE);
	return (ERROR);
}

static void		ft_check_path(t_type state, t_list *room_list, int ants)
{
	t_elem	*start;
	t_elem	*end;
	t_room	*current_room;

	start = NULL;
	end = NULL;
	(void)state;
	if (has_cmd(room_list, CMD_START, &start) == TRUE
		&& has_cmd(room_list, CMD_END, &end) == TRUE)
	{
		dijkstra(room_list, end->room, start->room);
		current_room = start->room;
		while (current_room != NULL)
		{
			if (current_room == end->room)
			{
				print_ants(start->room, end->room, ants);
				return ;
			}
			current_room = current_room->pred;
		}
	}
	ft_putendl_fd("ERROR", 2);
}

static void		main_loop(char *line, t_type *current, int *ants,
	t_list *room_list)
{
	t_type	old;

	old = NOTHING;
	if (*current != COMMENT)
		old = *current;
	*current = check_line(line, current, room_list);
	if (*current == NUMBER)
		*ants = ft_atoi(line);
	if (ants == 0)
		*current = ERROR;
	if (room_list->tail != NULL)
	{
		if (*current == ROOM && old == CMD_START)
			room_list->tail->room->type = CMD_START;
		else if (*current == ROOM && old == CMD_END)
			room_list->tail->room->type = CMD_END;
		else if (*current == ROOM)
			room_list->tail->room->type = *current;
	}
	if (*current != ERROR)
		ft_putendl(line);
	free(line);
}

int				main(void)
{
	char	*line;
	int		ants;
	t_type	current;
	t_type	old;
	t_list	*room_list;

	room_list = dlstnew();
	current = NOTHING;
	old = NOTHING;
	ants = 0;
	while (get_next_line(0, &line) == 1)
	{
		main_loop(line, &current, &ants, room_list);
		if (current == ERROR || current == STOP)
			break ;
	}
	ft_check_path(current, room_list, ants);
	dlstdel_list(&room_list, 0);
	return (0);
}
