/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   priority_queue.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mcizo <mcizo@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/04 22:44:36 by mcizo             #+#    #+#             */
/*   Updated: 2016/02/08 16:38:38 by mcizo            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <lemin.h>

t_room	*pq_extract_min(t_heap *heap)
{
	t_room	*result;

	result = heap->array[1];
	heap_delete_root(heap);
	result->enqueued = FALSE;
	return (result);
}

void	pq_decrease_priority(t_heap *heap, int i, int new_dist)
{
	heap->array[i]->dist = new_dist;
	heapify(heap, i);
}
