/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   dijkstra.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mcizo <mcizo@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/04 23:09:05 by mcizo             #+#    #+#             */
/*   Updated: 2016/02/08 17:22:14 by mcizo            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <limits.h>
#include <lemin.h>

static void		find_path(t_heap *priority_queue, t_room *target)
{
	long	dist;
	t_elem	*current;
	t_room	*u;

	current = NULL;
	while (priority_queue->size > 0)
	{
		u = pq_extract_min(priority_queue);
		if (u->connected_rooms && u->connected_rooms->head)
			current = u->connected_rooms->head;
		while (current != NULL)
		{
			dist = (u->dist == LONG_MAX) ? LONG_MAX : u->dist + 1;
			if (dist < current->room->dist)
			{
				if (current->room->enqueued == FALSE)
					insert_heap_node(priority_queue, current->room);
				current->room->dist = dist;
				current->room->pred = u;
				if (current->room == target)
					break ;
			}
			current = current->next;
		}
	}
}

void			dijkstra(t_list *room_list, t_room *source, t_room *target)
{
	t_elem		*current;
	t_heap		*priority_queue;

	source->dist = 0;
	priority_queue = init_min_heap();
	insert_heap_node(priority_queue, source);
	current = room_list->head;
	while (current != NULL)
	{
		if (current->room != source)
			current->room->dist = LONG_MAX;
		current->room->pred = NULL;
		current = current->next;
	}
	find_path(priority_queue, target);
	free_heap(priority_queue);
}
