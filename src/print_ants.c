/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_ants.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mcizo <mcizo@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/08 12:32:53 by mcizo             #+#    #+#             */
/*   Updated: 2016/02/08 16:41:10 by mcizo            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <lemin.h>

static void		print_step(t_room **ants_positions, t_bool *show_space,
	int i, t_room *start)
{
	t_room		*tmp;

	if (*show_space == TRUE)
		ft_putchar(' ');
	tmp = ants_positions[i];
	ants_positions[i] = (ants_positions[i] == NULL)
	? start->pred : ants_positions[i]->pred;
	ft_printf("L%d-%s", i + 1, ants_positions[i]->name);
	*show_space = TRUE;
}

static t_bool	empty_path(t_room **ants_positions, t_room *end, int ant_nb)
{
	int		i;

	i = 0;
	while (i < ant_nb)
	{
		if (ants_positions[i] != end)
			return (FALSE);
		i++;
	}
	return (TRUE);
}

void			print_ants(t_room *start, t_room *end, int ant_nb)
{
	t_room		**ants_positions;
	int			i;
	int			walking_ants;
	t_bool		show_space;

	ants_positions = ft_memalloc(sizeof(t_room *) * (ant_nb + 1));
	ants_positions[0] = start;
	walking_ants = 1;
	ft_putendl("");
	while (empty_path(ants_positions, end, ant_nb) == FALSE)
	{
		i = 0;
		show_space = FALSE;
		while (i < walking_ants)
		{
			if (ants_positions[i] != end)
				print_step(ants_positions, &show_space, i, start);
			i++;
		}
		ft_putendl("");
		if (walking_ants < ant_nb)
			walking_ants++;
	}
	free(ants_positions);
}
