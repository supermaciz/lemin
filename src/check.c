/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mcizo <mcizo@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/16 15:04:15 by mcizo             #+#    #+#             */
/*   Updated: 2016/02/06 12:33:31 by mcizo            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <lemin.h>

t_bool		check_number(char *line)
{
	static int	number = 0;
	int			i;

	if (line[0] == '-')
		return (FALSE);
	i = 0;
	while (line[i] != '\0' && ft_isdigit(line[i]) != 0)
		i++;
	if (line[i] != '\0')
		return (FALSE);
	else
		number++;
	if (number > 1)
		return (FALSE);
	return (TRUE);
}

t_bool		check_cmd(t_type cmd, char *line)
{
	static int	start = 0;
	static int	end = 0;

	if (cmd == CMD_START)
		start++;
	else if (cmd == CMD_END)
		end++;
	if (start > 1 || end > 1)
	{
		free(line);
		return (FALSE);
	}
	return (TRUE);
}

t_bool		check_room(char *line, t_type *current, t_list *room_list)
{
	char	**split;
	int		i;

	if (current == NULL)
		ft_putendl("null");
	if (line[0] == 'L')
		return (FALSE);
	split = ft_strsplit(line, ' ');
	i = 0;
	while (split[i] != 0)
		i++;
	if (i != 3 || ft_strchr(split[0], '-') || verif_split_room(split) == FALSE)
	{
		ft_array_del(split);
		return (FALSE);
	}
	dlstadd_end(room_list, new_room(split[0]));
	ft_array_del(split);
	return (TRUE);
}

static void	add_tube(t_elem *start, t_elem *dest)
{
	if (start->room->connected_rooms == NULL)
	{
		start->room->connected_rooms = dlstnew();
		dlstadd_end(start->room->connected_rooms, dest->room);
	}
	else if (verif_new_tube(start->room->connected_rooms,
		dest->room->name) == TRUE)
	{
		dlstadd_end(start->room->connected_rooms, dest->room);
	}
}

t_bool		check_tube(char *line, t_type *current, t_list *room_list)
{
	char	**split;
	t_elem	*fst;
	t_elem	*snd;

	if (current == NULL)
		ft_putendl("null");
	if (ft_strchr(line, ' '))
		return (FALSE);
	split = ft_strsplit(line, '-');
	if (verif_split_tube(split) == FALSE)
		return (FALSE);
	fst = room_list->head;
	while (fst != NULL && ft_strcmp(fst->room->name, split[0]) != 0)
		fst = fst->next;
	snd = room_list->head;
	while (snd != NULL && ft_strcmp(snd->room->name, split[1]) != 0)
		snd = snd->next;
	ft_array_del(split);
	if (!(fst != NULL && snd != NULL))
		return (FALSE);
	add_tube(fst, snd);
	add_tube(snd, fst);
	return (TRUE);
}
