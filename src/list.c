/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   list.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mcizo <mcizo@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/28 12:49:38 by mcizo             #+#    #+#             */
/*   Updated: 2016/02/08 12:45:47 by mcizo            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <lemin.h>
#include <stdlib.h>

t_room	*new_room(char *name)
{
	t_room *room;

	room = (t_room *)ft_memalloc(sizeof(*room));
	if (room != NULL)
	{
		room->name = ft_strdup(name);
		room->connected_rooms = NULL;
		room->enqueued = FALSE;
	}
	return (room);
}

t_list	*dlstnew(void)
{
	t_list	*new_list;

	new_list = (t_list *)ft_memalloc(sizeof(*new_list));
	if (new_list != NULL)
	{
		new_list->len = 0;
		new_list->head = NULL;
		new_list->tail = NULL;
	}
	return (new_list);
}

t_list	*dlstadd_end(t_list *lst, t_room *room)
{
	t_elem	*new_elem;

	if (lst != NULL)
	{
		new_elem = (t_elem *)ft_memalloc(sizeof(*new_elem));
		if (new_elem != NULL)
		{
			new_elem->room = room;
			new_elem->next = NULL;
			if (lst->tail == NULL)
			{
				new_elem->prev = NULL;
				lst->head = new_elem;
				lst->tail = new_elem;
			}
			else
			{
				lst->tail->next = new_elem;
				new_elem->prev = lst->tail;
				lst->tail = new_elem;
			}
			lst->len++;
		}
	}
	return (lst);
}

void	dlstdel_list(t_list **lst, int level)
{
	t_elem	*del;
	t_elem	*tmp;

	if (*lst != NULL)
	{
		tmp = (*lst)->head;
		while (tmp != NULL)
		{
			del = tmp;
			tmp = tmp->next;
			if (level == 0)
			{
				ft_strdel(&del->room->name);
				dlstdel_list(&del->room->connected_rooms, level + 1);
				free(del->room);
				del->room = NULL;
			}
			free(del);
		}
		free(*lst);
		*lst = NULL;
	}
}
