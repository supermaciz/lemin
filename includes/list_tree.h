/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   list_tree.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mcizo <mcizo@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/03 23:19:40 by mcizo             #+#    #+#             */
/*   Updated: 2016/02/08 17:28:37 by mcizo            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIST_TREE_H
# define LIST_TREE_H

# include <stddef.h>

typedef enum		e_type
{
	COMMENT, ROOM, TUBE, CMD_START, CMD_END, CMD_ERROR, ERROR, NUMBER,
	NOTHING, STOP
}					t_type;

typedef enum		e_bool
{
	FALSE, TRUE
}					t_bool;

typedef struct		s_room
{
	char			*name;
	struct s_list	*connected_rooms;
	t_type			type;
	t_bool			enqueued;
	struct s_room	*pred;
	long			dist;
	int				index;
}					t_room;

typedef struct		s_elem
{
	t_room			*room;
	struct s_elem	*next;
	struct s_elem	*prev;
}					t_elem;

typedef struct		s_list
{
	size_t			len;
	t_elem			*tail;
	t_elem			*head;
}					t_list;

typedef struct		s_heap
{
	int				capacity;
	int				size;
	t_room			**array;
}					t_heap;

t_list				*dlstnew(void);
t_list				*dlstadd_end(t_list *lst, t_room *room);
void				dlstdel_list(t_list **lst, int level);
t_elem				*dl_pop(t_list *stack);
t_room				*new_room(char *name);

int					heap_parent(int index);
int					heap_left_child(int index);
int					heap_right_child(int index);
int					heap_parent(int index);
void				insert_heap_node(t_heap *heap, t_room *node);
t_heap				*init_min_heap();
void				free_heap(t_heap *heap);
void				heap_delete_root(t_heap *heap);
void				heapify(t_heap *heap, int i);

t_room				*pq_extract_min(t_heap *heap);
void				pq_decrease_priority(t_heap *heap, int i, int new_dist);

#endif
