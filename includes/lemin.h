/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lemin.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mcizo <mcizo@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/12 15:59:00 by mcizo             #+#    #+#             */
/*   Updated: 2016/02/08 16:44:45 by mcizo            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LEMIN_H
# define LEMIN_H

# include <libft.h>
# include <list_tree.h>

t_bool	verif_split_room(char **split);
t_bool	verif_split_tube(char **split);
t_type	check_line(char *line, t_type *current, t_list *room_list);
t_bool	check_number(char *line);
t_bool	check_cmd(t_type cmd, char *line);
t_bool	check_room(char *line, t_type *current, t_list *room_list);
t_bool	check_tube(char *line, t_type *current, t_list *room_list);
t_bool	verif_new_tube(t_list *tube_list, char *new_tube);

void	dijkstra(t_list *room_list, t_room *source, t_room *target);
void	print_ants(t_room *start, t_room *end, int ant_nb);

#endif
