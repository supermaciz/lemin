/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   printf_structs.h                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mcizo <mcizo@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/04/26 16:18:32 by mcizo             #+#    #+#             */
/*   Updated: 2014/04/26 16:23:23 by mcizo            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PRINTF_STRUCTS_H
# define PRINTF_STRUCTS_H

typedef union			u_content
{
	int					i;
	unsigned int		u;
	long int			l;
	unsigned char		c;
	char				*str;
}						t_content;

typedef struct			s_param
{
	char				type;
	t_content			value;
}						t_param;

#endif
