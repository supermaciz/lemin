/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libft.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mcizo <mcizo@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/19 15:21:52 by mcizo             #+#    #+#             */
/*   Updated: 2015/12/28 13:43:06 by mcizo            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFT_H
# define LIBFT_H

# define BUFF_SIZE 1

# include <string.h>
# include <stdarg.h>
# include "printf_structs.h"

void	ft_putchar(char c);
void	ft_putstr(char const *s);
int		ft_tolower(int c);
int		ft_toupper(int c);
size_t	ft_strlen(const char *s);
void	*ft_memset(void *b, int c, size_t len);
char	*ft_strcpy(char *s1, const char *s2);
int		ft_isascii(int c);
int		ft_isalpha(int c);
int		ft_isdigit(int c);
int		ft_isalnum(int c);
int		ft_isprint(int c);
void	ft_putchar_fd(char c, int fd);
void	ft_putstr_fd(char const *s, int fd);
void	ft_putendl(char const *s);
void	ft_putendl_fd(char const *s, int fd);
char	*ft_strdup(const char *s1);
void	ft_bzero(void *s, size_t n);
void	ft_putnbr(int n);
char	*ft_strchr(const char *s, int c);
char	*ft_strrchr(const char *s, int c);
int		ft_atoi(const char *str);
void	ft_strdel(char **as);
void	*ft_memcpy(void *s1, const void *s2, size_t n);
void	*ft_memalloc(size_t size);
void	ft_strclr(char *s);
char	*ft_strncpy(char *s1, const char *s2, size_t n);
void	*ft_memccpy(void *s1, const void *s2, int c, size_t n);
void	*ft_memmove(void *s1, const void *s2, size_t n);
void	*ft_memchr(const void *s, int c, size_t n);
int		ft_memcmp(const void *s1, const void *s2, size_t n);
char	*ft_strcat(char *s1, const char *s2);
char	*ft_strncat(char *s1, const char *s2, size_t n);
size_t	ft_strlcat(char *dst, const char *src, size_t size);
char	*ft_strstr(const char *s1, const char *s2);
char	*ft_strnstr(const char *s1, const char *s2, size_t n);
int		ft_strcmp(const char *s1, const char *s2);
int		ft_strncmp(const char *s1, const char *s2, size_t n);
void	ft_memdel(void **ap);
char	*ft_strnew(size_t size);
void	ft_striter(char *s, void (*f)(char *));
void	ft_striteri(char *s, void (*f)(unsigned int, char *));
char	*ft_strmap(char const *s, char (*f)(char));
int		ft_strequ(char const *s1, char const *s2);
char	*ft_strmapi(char const *s, char (*f)(unsigned int, char));
int		ft_strnequ(char const *s1, char const *s2, size_t n);
char	*ft_strsub(char const *s, unsigned int start, size_t len);
char	*ft_strjoin(char const *s1, char const *s2);
char	*ft_strtrim(char const *s);
char	**ft_strsplit(char const *s, char c);
char	*ft_itoa(int n);
void	ft_putnbr_fd(int n, int fd);
int		ft_abs(int value);
int		get_next_line(int const fd, char **line);
char	**ft_array_dup(void *array_ptr);
void	ft_array_del(char **array_ptr);
char	*ft_strreplace(char *s1, char *s2);
char	*ft_append(char *str, char c);
float	ft_fabs(float value);

int		ft_printf(const char *format, ...);
void	print_integer(t_param *printf_param, int *len);
void	print_str(t_param *printf_param, int *len);
void	print_char(t_param *printf_param, int *len);
void	print_format(const char *format, int *index, int *len);
void	print_u_int(t_param *printf_arg, int *len);
char	*ft_uitoa(unsigned int n);
void	print_oct_int(t_param *printf_param, int *len);

int		ft_printf_fd(int fd, const char *format, ...);
void	print_integer_fd(t_param *printf_param, int *len);
void	print_str_fd(t_param *printf_param, int *len);
void	print_char_fd(t_param *printf_param, int *len);
void	print_format_fd(const char *format, int *index, int *len);
void	print_u_int_fd(t_param *printf_arg, int *len);
void	print_oct_int_fd(t_param *printf_param, int *len);
int		get_printf_fd(int fd);

#endif
