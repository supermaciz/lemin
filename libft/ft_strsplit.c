/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplit.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mcizo <mcizo@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/29 21:49:33 by mcizo             #+#    #+#             */
/*   Updated: 2014/04/19 14:59:39 by mcizo            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"

static int	count_str(char const *s, char c)
{
	int	nb;
	int	i;

	nb = 0;
	i = 0;
	while (s[i] != '\0')
	{
		if (s[i] != c)
		{
			nb++;
			while (s[i] != c)
			{
				if (!s[i])
					return (nb);
				i++;
			}
		}
		i++;
	}
	return (nb);
}

static void	increment_var(size_t *len, unsigned int *i)
{
	*len += 1;
	*i += 1;
}

char		**ft_strsplit(char const *s, char c)
{
	unsigned int	start;
	unsigned int	i;
	int				i2;
	size_t			len;
	char			**splitted_str;

	i = 0;
	i2 = 0;
	splitted_str = malloc((count_str(s, c) + 1) * sizeof(char *));
	while (i < (unsigned int)ft_strlen(s))
	{
		if (s[i] != c)
		{
			len = 0;
			start = i;
			while (s[i] != c && i < ft_strlen(s))
				increment_var(&len, &i);
			splitted_str[i2++] = ft_strsub(s, start, len);
		}
		i++;
	}
	if (count_str(s, c) == 0)
		return (ft_memalloc(1));
	splitted_str[count_str(s, c)] = 0;
	return (splitted_str);
}
