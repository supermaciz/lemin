/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mcizo <mcizo@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/21 14:29:26 by mcizo             #+#    #+#             */
/*   Updated: 2013/11/22 11:13:06 by mcizo            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static void	ft_putnbr_2(int n)
{
	if (n < 0)
	{
		ft_putchar('-');
		n *= -1;
	}
	if (n)
	{
		ft_putnbr(n / 10);
		ft_putchar(n % 10 + 48);
	}
}

void		ft_putnbr(int n)
{
	if (n < 10 && n >= 0)
	{
		ft_putchar(n + 48);
	}
	else if (n > -10 && n < 0)
	{
		ft_putchar('-');
		n *= -1;
		ft_putchar(n + 48);
	}
	else
	{
		ft_putnbr_2(n);
	}
}
