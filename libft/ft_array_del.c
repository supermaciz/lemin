/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_array_del.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mcizo <mcizo@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/12 15:48:21 by mcizo             #+#    #+#             */
/*   Updated: 2016/01/18 16:34:20 by mcizo            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

void	ft_array_del(char **array)
{
	int		i;

	i = 0;
	if (array != NULL)
	{
		while (array[i] != NULL)
		{
			ft_strdel(&array[i]);
			i++;
		}
		free(array);
	}
	array = NULL;
}
