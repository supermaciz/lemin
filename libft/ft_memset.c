/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memset.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mcizo <mcizo@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/19 22:31:20 by mcizo             #+#    #+#             */
/*   Updated: 2015/01/20 18:48:57 by mcizo            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memset(void *b, int c, size_t len)
{
	unsigned char	value;
	size_t			i;
	char			*str;

	value = c;
	i = 0;
	str = (char *)b;
	while (i < len)
	{
		str[i] = value;
		i++;
	}
	return (b);
}
