/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mcizo <mcizo@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/22 17:57:31 by mcizo             #+#    #+#             */
/*   Updated: 2014/02/01 21:11:43 by mcizo            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strstr(const char *s1, const char *s2)
{
	size_t	s2len;

	s2len = ft_strlen(s2);
	if (!s2len || s2[0] == '\0')
		return ((char *)s1);
	while (s1[0] != '\0')
	{
		if (ft_memcmp(s1, s2, s2len - 1) == 0)
			return ((char *)s1);
		s1++;
	}
	return (NULL);
}
