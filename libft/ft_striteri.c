/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_striteri.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mcizo <mcizo@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/27 14:33:34 by mcizo             #+#    #+#             */
/*   Updated: 2013/12/04 18:22:35 by mcizo            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_striteri(char *s, void (*f)(unsigned int, char *))
{
	unsigned int	i;
	unsigned int	slen;

	i = 0;
	slen = (unsigned int)ft_strlen(s);
	if (s != NULL && f != NULL)
	{
		while (i < slen)
		{
			f(i, s + i);
			i++;
		}
	}
}
