/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf_fd2.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mcizo <mcizo@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/14 07:57:22 by mcizo             #+#    #+#             */
/*   Updated: 2015/02/05 05:24:08 by mcizo            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

void		print_char_fd(t_param *printf_arg, int *len)
{
	ft_putchar_fd((printf_arg->value.c), get_printf_fd(-1));
	*len += 1;
}

void		print_integer_fd(t_param *printf_arg, int *len)
{
	char	*number;

	number = NULL;
	number = ft_itoa(printf_arg->value.i);
	ft_putstr_fd(number, get_printf_fd(-1));
	*len += ft_strlen(number);
	if (number != NULL)
		free(number);
}

void		print_u_int_fd(t_param *printf_arg, int *len)
{
	char	*number;

	number = NULL;
	number = ft_uitoa(printf_arg->value.u);
	ft_putstr_fd(number, get_printf_fd(-1));
	*len += ft_strlen(number);
	if (number != NULL)
		free(number);
}

void		print_str_fd(t_param *printf_arg, int *len)
{
	if (printf_arg->value.str == NULL)
		printf_arg->value.str = "(null)";
	ft_putstr_fd(printf_arg->value.str, get_printf_fd(-1));
	*len += ft_strlen(printf_arg->value.str);
}

void		print_format_fd(const char *format, int *index, int *len)
{
	ft_putchar_fd(format[*index], get_printf_fd(-1));
	*index += 1;
	*len += 1;
}
