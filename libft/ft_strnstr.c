/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mcizo <mcizo@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/22 17:59:43 by mcizo             #+#    #+#             */
/*   Updated: 2013/11/28 19:06:27 by mcizo            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strnstr(const char *s1, const char *s2, size_t n)
{
	int	i;
	int	i2;

	i = 0;
	i2 = 0;
	if (s2[0] == '\0')
	{
		return ((char *)s1);
	}
	while (s1[i] != '\0' && i2 < (int)ft_strlen(s2) && i < (int)n)
	{
		if (s1[i] == s2[i2])
		{
			i2++;
		}
		i++;
	}
	if (i2 == (int)ft_strlen(s2))
	{
		return ((char *)s1 + i - i2);
	}
	else
	{
		return (NULL);
	}
}
