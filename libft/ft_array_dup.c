/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_array_dup.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mcizo <mcizo@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/12 15:48:21 by mcizo             #+#    #+#             */
/*   Updated: 2015/11/28 15:59:45 by mcizo            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

char	**ft_array_dup(void *array_ptr)
{
	char			**array;
	char			**dup;
	size_t			i;

	array = (char **)array_ptr;
	i = 0;
	while (array[i])
		i++;
	dup = (char **)ft_memalloc(sizeof(char *) * (i + 1));
	i = 0;
	while (array[i])
	{
		dup[i] = ft_strdup(array[i]);
		i++;
	}
	dup[i] = 0;
	return (dup);
}
