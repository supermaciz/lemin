/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mcizo <mcizo@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/22 17:38:13 by mcizo             #+#    #+#             */
/*   Updated: 2013/11/27 12:52:20 by mcizo            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memchr(const void *s, int c, size_t n)
{
	char	*str;
	size_t	i;

	str = (char *)s;
	i = 0;
	if (s != NULL)
	{
		while (i < n)
		{
			if (str[i] == (unsigned char)c)
			{
				return ((void *)(s + i));
			}
			i++;
		}
	}
	return (NULL);
}
