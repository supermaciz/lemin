/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mcizo <mcizo@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/02 18:13:21 by mcizo             #+#    #+#             */
/*   Updated: 2015/02/01 22:29:29 by mcizo            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <sys/types.h>
#include <sys/uio.h>
#include <unistd.h>
#include <stdlib.h>
#include <libft.h>

static int		start_new_line(char **saved_str, char *new_line)
{
	char	*tmp;

	tmp = NULL;
	if (saved_str != NULL && saved_str[0] != '\0')
	{
		if (new_line)
			new_line++;
		if (new_line != NULL && new_line[0] != '\0')
			tmp = ft_strdup(new_line);
		ft_strdel(saved_str);
		*saved_str = tmp;
		return (1);
	}
	else
		return (0);
}

static char		*save_buf(char **tmp, char *buf)
{
	char	*tmp2;

	if (*tmp == NULL)
		*tmp = ft_strdup(buf);
	else
	{
		tmp2 = ft_strjoin(*tmp, buf);
		ft_strdel(tmp);
		*tmp = tmp2;
	}
	return (*tmp);
}

static int		set_line(char *tmp, char **line)
{
	size_t	len;
	char	*newline;

	if (!tmp)
		return (0);
	newline = ft_strchr(tmp, '\n');
	len = (newline != NULL) ? (size_t)(newline - tmp) : ft_strlen(tmp);
	*line = ft_strnew(len + 1);
	*line = ft_strncpy(*line, tmp, len);
	if (newline == NULL)
		return (0);
	else
		return (1);
}

int				get_next_line(int const fd, char **line)
{
	char		*buf;
	static char	*saved_str = NULL;
	int			ret;
	char		*new_line;

	if (BUFF_SIZE <= 0 || line == NULL)
		return (-1);
	buf = ft_strnew(BUFF_SIZE);
	while ((ret = read(fd, buf, BUFF_SIZE)) > 0)
	{
		buf[ret] = '\0';
		saved_str = save_buf(&saved_str, buf);
		if ((new_line = ft_strchr(saved_str, '\n')) != NULL)
			break ;
	}
	new_line = (saved_str != NULL) ? ft_strchr(saved_str, '\n') : NULL;
	ft_strdel(&buf);
	if (ret >= 0 && saved_str != NULL)
	{
		set_line(saved_str, line);
		ret = start_new_line(&saved_str, new_line);
		if (ret == 0)
			ft_strdel(&saved_str);
	}
	return (ret);
}
