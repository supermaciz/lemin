# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: mcizo <mcizo@student.42.fr>                +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2014/01/07 22:44:15 by mcizo             #+#    #+#              #
#    Updated: 2016/02/08 16:44:36 by mcizo            ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = lem-in
CC = cc
CFLAGS = -Wall -Werror -Wextra -g3
LIBFTDIR = ./libft/
LFLAGS = -L$(LIBFTDIR) -lft
INC_DIR = ./includes
INC_LIBFT_DIR = ./libft/includes
OBJS_DIR = objects
SRCS_DIR = src
SRCS = main.c list.c verif.c check.c binary_heap.c bin_heap2.c \
	priority_queue.c dijkstra.c print_ants.c
OBJS = $(patsubst %.c, $(OBJS_DIR)/%.o, $(SRCS))

all: $(NAME)

$(NAME): $(OBJS_DIR) $(OBJS)
	$(CC) -o $(NAME) $(OBJS) $(LFLAGS)

$(OBJS_DIR)/%.o: $(addprefix $(SRCS_DIR)/, %.c)
	$(CC) $(CFLAGS) -o $@ -c $^ -I $(INC_DIR) -I $(INC_LIBFT_DIR)

$(OBJS_DIR): makelibft
	@mkdir -p $(OBJS_DIR)

.PHONY: makelibft fclean clean re

makelibft:
	$(MAKE) -C $(LIBFTDIR)

fclean: clean
	$(MAKE) -C $(LIBFTDIR) fclean
	rm -f $(NAME)

clean:
	$(MAKE) -C $(LIBFTDIR) clean
	rm -rf $(OBJS_DIR)

re: fclean all
